﻿using System;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Player : MonoBehaviour 
{
	[SerializeField] private Rigidbody _rigidbody;
	[SerializeField] private RigidbodyFirstPersonController _fpsController;

	public Rigidbody Rigidbody { get { return _rigidbody; } }
	public RigidbodyFirstPersonController FpsController { get { return _fpsController; } }

	private void Start()
	{
		GameMaster.Instance.CheckpointReached(transform.position);
	}

	public void Die()
	{
		GameMaster.Instance.Respawn();
	}
}

