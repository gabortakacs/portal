﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class WeaponChanger : MonoBehaviour 
{

	[SerializeField] private WeaponBind[] _weapons;

	private Dictionary<KeyCode, Weapon> _weaponBindDir = new Dictionary<KeyCode, Weapon>();

	private void Awake()
	{
		foreach(WeaponBind weaponBind in _weapons)
		{
			_weaponBindDir[weaponBind.KeyBind] = weaponBind.Weapon;
		}
	}

	private void Update()
	{
		foreach(KeyValuePair<KeyCode, Weapon> weapon in _weaponBindDir)
		{
			if (Input.GetKeyDown(weapon.Key))
			{
				ChangeWeapon(weapon.Value);
			}
		}
	}

	private void ChangeWeapon(Weapon activeWeapon)
	{
		foreach(WeaponBind weaponBind in _weapons)
		{
			weaponBind.Weapon.gameObject.SetActive(weaponBind.Weapon == activeWeapon);
		}
	}
}

[Serializable]
public struct WeaponBind
{
	public KeyCode KeyBind;
	public Weapon Weapon;
}