﻿using System;
using UnityEngine;

public class WeaponTelekinesis : Weapon 
{

	[SerializeField] private TelekinesisTrigger _trigger;
	[SerializeField] private Renderer _triggerRenderer;
	[SerializeField] private float _triggerMaxSize = 10.0f;
	[SerializeField] private float _triggerIncreaseSpeed = 2.0f;

	private Vector3 _triggerOriginalSize;
	private bool _blocked = false;

	private void Awake()
	{
		_triggerOriginalSize = _trigger.transform.localScale;
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			_trigger.gameObject.SetActive(true);
		}
		else if (Input.GetMouseButton(0) && !_blocked)
		{
			Shoot();
		}
		else if (Input.GetMouseButtonUp(0))
		{
			if(_blocked)
				_blocked = false;
			else
				ClearShoot();
		}
	}

	private void Shoot()
	{
		Vector3 scale = _trigger.transform.localScale;
		scale.z += Time.deltaTime * _triggerIncreaseSpeed * _triggerMaxSize;
		scale.z = Mathf.Min(scale.z, _triggerMaxSize);
		if (scale.z == _triggerMaxSize && !_trigger.HasTarget)
		{
			ClearShoot();
			_blocked = true;
			return;
		}
		_trigger.transform.localScale = scale;
	}

	private void ClearShoot()
	{
		_trigger.ClearTarget();
		_trigger.transform.localScale = _triggerOriginalSize;
		_trigger.gameObject.SetActive(false);
	}
}

