﻿using System;
using UnityEngine;

public class WeaponPortal : Weapon 
{
	[SerializeField] private PortalBullet[] _portalBullets;

	private bool _firstPortalActive = true;

	private void Awake()
	{
		for(int i = 0; i < _portalBullets.Length; ++i)
		{
			_portalBullets[i].Init(i);
		}
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(1))
		{
			ChangePortal();
		}
		if (Input.GetMouseButtonDown(0))
		{
			Shoot();
		}
	}

	private void Shoot()
	{
		_portalBullets[_firstPortalActive ? 0 : 1].Shoot();
	}

	private void ChangePortal()
	{
		_firstPortalActive = !_firstPortalActive;
		if(_firstPortalActive)
		{
			_portalBullets[0].gameObject.SetActive(true);
			_portalBullets[1].Deactivate();
		}
		else
		{
			_portalBullets[0].Deactivate();
			_portalBullets[1].gameObject.SetActive(true);
		}
	}
}

