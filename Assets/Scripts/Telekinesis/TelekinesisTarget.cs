﻿using UnityEngine;

public enum TelekinesisTargetType
{
	Moveable = 1,
	Rotatory = 2
}

public abstract class TelekinesisTarget : MonoBehaviour 
{
	[SerializeField] protected TelekinesisTargetType _type;
	public TelekinesisTargetType Type { get { return _type; } }

	public abstract void ClearTarget();
	public abstract void Targeted(Transform parent);
}

