﻿using UnityEngine;

public class TelekinesisTrigger : MonoBehaviour 
{
	[SerializeField] private Transform _targetedObjectParent;

	private TelekinesisTarget _target;

	public bool HasTarget { get { return _target != null; } }

	private void OnTriggerEnter(Collider other)
	{
		TelekinesisTarget target = other.GetComponent<TelekinesisTarget>();
		ClearTarget();
		SetTarget(target);
	}

	private void SetTarget(TelekinesisTarget target)
	{
		_target = target;
		target.Targeted(_targetedObjectParent);
	}

	public void ClearTarget()
	{
		if (_target == null)
			return;
		_target.ClearTarget();
		_target = null;
	}
}

