﻿using UnityEngine;

public class TelekinesisTargetRemovable : TelekinesisTarget 
{
	[SerializeField] private Rigidbody _rigidbody;

	private Transform _originalParent;
	private Vector3 _originalLocalScale;
	private bool _originalGravity;
	private bool _isTargeted = false;

	private Vector3 _parentLastPos;

	private void Awake()
	{
		_originalParent = transform.parent;
		_originalLocalScale = transform.localScale;
		_originalGravity = _rigidbody.useGravity;
	}

	private void Update()
	{
		if (!_isTargeted)
			return;

		Vector3 parentDisplacement = transform.parent.position - _parentLastPos;
		transform.position = transform.position + parentDisplacement;
		_parentLastPos = transform.parent.position;
	}

	private void FixedUpdate()
	{
		if (!_isTargeted)
			return;

		_rigidbody.velocity = Vector3.zero;
	}

	public override void ClearTarget()
	{
		UnTargeted();
	}

	public override void Targeted(Transform parent)
	{
		_isTargeted = true;
		_rigidbody.useGravity = false;
		transform.parent = parent;
		_parentLastPos = parent.transform.position;
	}

	private void UnTargeted()
	{
		_rigidbody.useGravity = _originalGravity;
		transform.parent = _originalParent;
		transform.localScale = _originalLocalScale;
		_isTargeted = false;
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (!_isTargeted || collision.gameObject.layer == LayerMask.NameToLayer("TelekinesisTrigger") || collision.gameObject.layer == LayerMask.NameToLayer("Player"))
			return;

		UnTargeted();
	}





}

