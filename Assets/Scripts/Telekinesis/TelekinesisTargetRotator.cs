﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class TelekinesisTargetRotator : TelekinesisTarget
{
	[SerializeField][Range(0f,360f)] private float _rotationSpeed = 45.0f;
	private Player _player;
	private Transform _playerOriginalParent;
	private bool _isTargeted = false;

	private void Start()
	{
		_player = GameMaster.Instance.Player;
		_playerOriginalParent = _player.transform.parent;
	}

	private void Update()
	{
		if (!_isTargeted)
			return;

		float horizontal = CrossPlatformInputManager.GetAxis("Mouse X");
		Quaternion addRotation =  Quaternion.AngleAxis(_rotationSpeed * Time.fixedDeltaTime * horizontal, Vector3.up);
		Vector3 eulerRotation = transform.rotation.eulerAngles;
		eulerRotation.y += addRotation.eulerAngles.y;
		transform.rotation = Quaternion.Euler(eulerRotation);
	}

	public override void Targeted(Transform parent)
	{
		_player.transform.parent = transform;
		_player.FpsController.enabled = false;
		_player.Rigidbody.useGravity = false;
		_isTargeted = true;
	}

	public override void ClearTarget()
	{
		_player.FpsController.enabled = true;
		_player.Rigidbody.useGravity = true;
		_player.transform.parent = _playerOriginalParent;
		Quaternion rotation = _player.transform.rotation;
		_player.FpsController.SetRotationY(rotation.eulerAngles.y);
		_isTargeted = false;
	}
}

