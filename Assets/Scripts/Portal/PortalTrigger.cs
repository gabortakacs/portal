﻿using System;
using UnityEngine;

public class PortalTrigger : MonoBehaviour 
{
	[SerializeField] private Transform _spawnPoint;
	private PortalTrigger _otherTrigger;
	private bool _used = false;

	public PortalTrigger OtherPortalTrigger { set { _otherTrigger = value; } }
	public Transform SpawnPoint { get { return _spawnPoint; } }
	public bool Used { set { _used = value; } }

	private void OnTriggerEnter(Collider other)
	{
		if (!PortalMaster.Instance.ArePortalsActive || _used)
			return;
		_used = true;
		_otherTrigger.Used = true;
		MoveObject(other.gameObject);
		RotateObject(other.gameObject);
	}

	private void RotateObject(GameObject obj)
	{
		Rigidbody rigidbody;
		if (obj.layer == LayerMask.NameToLayer("Player"))
		{
			Player player = obj.GetComponent<Player>();
			if (player == null)
				return;

			player.FpsController.SetRotationY(_otherTrigger.SpawnPoint.rotation.eulerAngles.y);
			rigidbody = player.Rigidbody;
		}
		else
		{
			obj.transform.rotation = _otherTrigger.SpawnPoint.rotation;
			rigidbody = obj.GetComponent<Rigidbody>();
		}
		SetRigidbodyVelocity(rigidbody);
	}

	private void SetRigidbodyVelocity(Rigidbody rigidbody)
	{
		if (rigidbody == null)
			return;
		rigidbody.velocity = rigidbody.transform.forward * rigidbody.velocity.magnitude;
	}

	private void MoveObject(GameObject obj)
	{
		obj.gameObject.transform.position = _otherTrigger.SpawnPoint.position;
	}

	private void OnTriggerExit(Collider other)
	{
		_used = false;
	}

}

