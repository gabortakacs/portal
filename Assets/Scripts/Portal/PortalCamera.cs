﻿using System;
using UnityEngine;

public class PortalCamera : MonoBehaviour 
{

	[SerializeField] private Transform _portal;

	private Transform _playerCamera;
	private Transform _otherPortal;

	public Transform PlayerCamera { set { _playerCamera = value; } }
	public Transform OtherPortal { set { _otherPortal = value; } }

	private void Update()
	{
		RotateCamera();
	}

	private void RotateCamera()
	{
		if (_otherPortal == null)
			return;
		Vector3 playerOffset = (_playerCamera.position - _otherPortal.position).normalized;
		playerOffset.y = 0;
		float angularDiff = Vector3.SignedAngle(_otherPortal.forward, playerOffset, Vector3.up);
		Quaternion portalRotation = Quaternion.AngleAxis(angularDiff, Vector3.up);
		Vector3 newCamDir = portalRotation * _portal.forward;
		newCamDir = Vector3.Reflect(newCamDir, _portal.right);
		transform.rotation = Quaternion.LookRotation(newCamDir, Vector3.up);
	}
}

