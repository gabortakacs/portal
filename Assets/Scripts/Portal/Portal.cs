﻿using System;
using UnityEngine;

public class Portal : MonoBehaviour
{
	[SerializeField] private PortalCamera _portalCamera;
	[SerializeField] private GameObject _portalGate;
	[SerializeField] private GameObject _borderBase;
	[SerializeField] private GameObject _borderAdditional;
	[SerializeField] private PortalTrigger _portalTrigger;

	public PortalTrigger PortalTrigger { get { return _portalTrigger; } }

	public void Init(PortalMaterials portalMaterials)
	{
		SetMaterials(portalMaterials);
		_portalCamera.PlayerCamera = GameMaster.Instance.PlayerCamera.transform;
	}

	public void SetOtherPortal(Portal otherPortal)
	{
		_portalCamera.OtherPortal = otherPortal.transform;
		_portalTrigger.OtherPortalTrigger = otherPortal.PortalTrigger;
	}

	private void SetMaterials(PortalMaterials portalMaterials)
	{
		_portalCamera.gameObject.GetComponent<Camera>().targetTexture = portalMaterials.CameraRenderTexture;
		_portalGate.gameObject.GetComponent<Renderer>().material = portalMaterials.GateCameraMaterial;
		_borderBase.gameObject.GetComponent<Renderer>().material = portalMaterials.BorderBaseMaterial;
		_borderAdditional.gameObject.GetComponent<Renderer>().material = portalMaterials.BorderAdditionMaterial;
	}
}

