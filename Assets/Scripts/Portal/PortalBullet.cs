﻿using System.Collections;
using UnityEngine;

public class PortalBullet : MonoBehaviour 
{
	[SerializeField] private Collider _collider;
	[SerializeField] private Rigidbody _rigidbody;
	[SerializeField] private Transform _launchPoint;
	[SerializeField] private float _speed = 20;

	Portal _portal;
	Vector3 _originalPos;
	Transform _parent;

	Coroutine _shootRoutine;

	private void Awake()
	{
		_originalPos = transform.localPosition;
		_parent = transform.parent;
		DoReset();
	}

	private void OnCollisionEnter(Collision collision)
	{
		if(collision.gameObject.layer == LayerMask.NameToLayer("PortalWall"))
			ReplacePortal(collision.gameObject, collision);
		DoReset();
	}

	public void Init(int index)
	{
		_portal = PortalMaster.Instance.GetPortal(index);
		_portal.gameObject.SetActive(false);
	}

	public void Deactivate()
	{
		DoReset();
		gameObject.SetActive(false);
	}

	public void Shoot()
	{
		_portal.gameObject.SetActive(false);
		_collider.enabled = true;
		transform.parent = null;
		_rigidbody.isKinematic = false;
		_rigidbody.AddForce(_launchPoint.forward * _speed, ForceMode.VelocityChange);
		if (_shootRoutine != null)
			StopCoroutine(_shootRoutine);
		_shootRoutine = StartCoroutine(ShootRoutine());
	}

	private void ReplacePortal(GameObject target, Collision collision)
	{
		_portal.gameObject.SetActive(true);
		_portal.gameObject.transform.position = collision.contacts[0].point;
		Vector3 rotation = target.transform.rotation.eulerAngles;
		rotation.y += 180;
		_portal.gameObject.transform.rotation = Quaternion.Euler(rotation);
		Vector3 localPos = _portal.gameObject.transform.localPosition;
		localPos += _portal.gameObject.transform.forward * 0.025f;
		_portal.gameObject.transform.localPosition = localPos;
	}

	private void DoReset()
	{
		if (_shootRoutine != null)
			StopCoroutine(_shootRoutine);
		_rigidbody.isKinematic = true;
		_collider.enabled = false;
		_rigidbody.velocity = Vector3.zero;
		transform.parent = _parent;
		transform.localPosition = _originalPos;
	}

	private IEnumerator ShootRoutine()
	{
		yield return new WaitForSeconds(3);
		DoReset();
	}

}

