﻿using System;
using UnityEngine;

public class PortalMaster : Singleton<PortalMaster> 
{

	[SerializeField] private Portal _portalPrefab;
	[SerializeField] private PortalMaterials[] _portalMaterials;

	private Portal[] _portals;

	public bool ArePortalsActive
	{
		get
		{
			foreach(Portal portal in _portals)
			{
				if (!portal.gameObject.activeSelf)
					return false;
			}
			return true;
		}
	}

	private void Awake()
	{
		_portals = new Portal[2];
	}

	public Portal GetPortal(int index)
	{
		if (_portals[index] == null)
		{
			_portals[index] = Instantiate(_portalPrefab);
			_portals[index].Init(_portalMaterials[index]);
			SetOtherPortals();
		}
		return _portals[index];
	}

	private void SetOtherPortals()
	{
		if (_portals[0] == null || _portals[1] == null)
			return;

		_portals[0].SetOtherPortal(_portals[1]);
		_portals[1].SetOtherPortal(_portals[0]);
	}
}

[Serializable]
public struct PortalMaterials
{
	public Material BorderBaseMaterial;
	public Material BorderAdditionMaterial;
	public Material GateCameraMaterial;
	public RenderTexture CameraRenderTexture;
}