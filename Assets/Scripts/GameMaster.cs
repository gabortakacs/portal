﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMaster : Singleton<GameMaster> 
{
	[SerializeField] private Player _player;
	[SerializeField] private Transform _playerCamera;

	private Vector3 _checkPoint = Vector3.positiveInfinity;

	public Player Player { get { return _player; } }
	public Transform PlayerCamera { get { return _playerCamera; } }

	public void Respawn()
	{
		if (_checkPoint == Vector3.positiveInfinity)
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		else
			_player.transform.position = _checkPoint;
	}

	public void CheckpointReached(Vector3 spawnpoint)
	{
		_checkPoint = spawnpoint;
	}

}

