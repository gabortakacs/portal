﻿using UnityEngine;

public class Checkpoint : MonoBehaviour 
{

	[SerializeField] private Transform _spawnpoint;

	private void OnTriggerEnter()
	{
		GameMaster.Instance.CheckpointReached(_spawnpoint.position);
		Destroy(gameObject);
	}



}

