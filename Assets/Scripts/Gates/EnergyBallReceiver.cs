﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBallReceiver : GateLogicSwitch
{
	[SerializeField] protected Material _onMaterial;
	public void TurnOn()
	{
		if (_state)
			return;

		_state = true;

		StateChanged();
		ChanceMaterial();
	}

	protected void ChanceMaterial()
	{
		_renderer.material = _onMaterial;
	}
}

