﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PushButton : GateLogicSwitch 
{

	[SerializeField] private Material _material;
	[SerializeField] private Transform _button;
	[SerializeField] private Transform _pushedPosition;
	[SerializeField] private float _speed;
	[SerializeField] private float _shaderModifierMaxValue = 1.0f;
	[SerializeField] private String _shaderModifierString = "Vector1_E201381";
	[SerializeField] private String _shaderColorString = "Color_F88A22BF";
	[SerializeField] private Gradient _color;

	private Vector3 _originalPosition;
	private int _pushedCounter = 0;
	private bool _isMoving = false;
	private bool _currentGoalDown;
	private float _maxDistanceBetweenUpAndDown;

	private bool IsPushed { get { return _pushedCounter > 0; } }
	public bool IsDown { get { return _state; } }

	private void Awake()
	{
		_originalPosition = _button.position;
		_renderer.material = Instantiate(_material);
		_maxDistanceBetweenUpAndDown = Vector3.Distance(_originalPosition, _pushedPosition.position);
		ChangeMaterial();
	}

	private void Update()
	{
		if(_isMoving)
		{
			MoveButton();
		}
		else
		{
			_isMoving = IsPushed && _button.position == _originalPosition || !IsPushed && _button.position == _pushedPosition.position;
			if(_isMoving)
				_currentGoalDown = IsPushed;
		}

		bool currentDownState = IsPushed && _currentGoalDown && !_isMoving;
		if(_state != currentDownState)
		{
			_state = currentDownState;
			StateChanged();
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer != LayerMask.NameToLayer("Player") && other.gameObject.layer != LayerMask.NameToLayer("TelekinesisTarget"))
			return;

		++_pushedCounter;
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.layer != LayerMask.NameToLayer("Player") && other.gameObject.layer != LayerMask.NameToLayer("TelekinesisTarget"))
			return;

		--_pushedCounter;
	}

	private void MoveButton()
	{
		if (_currentGoalDown)
		{
			_button.position = Vector3.MoveTowards(_button.position, _pushedPosition.position, _speed * Time.deltaTime);
			if (_button.position == _pushedPosition.position)
				_isMoving = false;
		}
		else
		{
			_button.position = Vector3.MoveTowards(_button.position, _originalPosition, _speed * Time.deltaTime);
			if (_button.position == _originalPosition)
				_isMoving = false;
		}
		ChangeMaterial();
	}

	private void ChangeMaterial()
	{

		float currentDistance = Vector3.Distance(_pushedPosition.position, _button.position);
		float prop = currentDistance / _maxDistanceBetweenUpAndDown;
		_renderer.material.SetFloat(_shaderModifierString, prop * _shaderModifierMaxValue);
		_renderer.material.SetColor(_shaderColorString, _color.Evaluate(prop));
	}
}

