﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class DoorShaderModifier : MonoBehaviour 
{

	[SerializeField] private Renderer _renderer;
	[SerializeField] private Material _material;
	[SerializeField] private Vector2 _stepMinMaxValue;
	[SerializeField] [Range(0, 360)] int _alphaRotation;
	[SerializeField] private string _voronoiOffsetMultiplierString = "Vector1_9D91A540";
	[SerializeField] private string _alphaRotationString = "Vector1_D3254C63";
	[SerializeField] private string _stepValueString = "Vector1_C7993EA6";

	private void Awake()
	{
		_renderer.material = Instantiate(_material);
		float voronoiOffsetMultiplier = Random.Range(0f, 10f);
		_renderer.material.SetFloat(_voronoiOffsetMultiplierString, voronoiOffsetMultiplier);
		_renderer.material.SetFloat(_alphaRotationString, _alphaRotation);
		UpdateStepValue(1);
	}

	public void UpdateStepValue(float newStepValue)
	{
		float step = Mathf.Lerp(_stepMinMaxValue.x, _stepMinMaxValue.y, newStepValue);
		_renderer.material.SetFloat(_stepValueString, step);

	}

}


