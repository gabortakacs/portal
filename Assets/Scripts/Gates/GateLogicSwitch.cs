﻿using System.Collections.Generic;
using UnityEngine;

public abstract class GateLogicSwitch : MonoBehaviour
{
	[SerializeField] protected Renderer _renderer;

	protected List<Gate> _linkedGates = new List<Gate>();
	protected bool _state = false;

	public void AddLinkedGate(Gate gate)
	{
		_linkedGates.Add(gate);
	}

	protected void StateChanged()
	{
		foreach (Gate gate in _linkedGates)
		{
			gate.SwitchStateChanged(_state);
		}
	}
}

