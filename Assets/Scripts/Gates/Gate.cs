﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public enum GateState
{
	Closed = 1,
	Moving = 2,
	Open = 3
}

public class Gate : MonoBehaviour
{
	[SerializeField] private DoorStruct[] _doors;
	[SerializeField] private GateLogicSwitch[] _buttons;
	[SerializeField] private float _doorMovingSpeed = 5.0f;

	private int _buttonPushedCounter = 0;
	private GateState _gateState;
	private Coroutine _doorMovingRoutine;

	private void Awake()
	{
		for (int i = 0; i < _doors.Length; ++i)
		{
			_doors[i].CloseStatePosition = _doors[i].Collider.transform.position;
			_doors[i].MaxDistance = Vector3.Distance(_doors[i].CloseStatePosition, _doors[i].OpenStatePosition.position);
		}
	}

	private void Start()
	{
		for(int i = 0; i < _buttons.Length; ++i)
		{
			_buttons[i].AddLinkedGate(this);
		}
	}

	public void SwitchStateChanged(bool lastDownState)
	{
		if (!lastDownState)
		{
			_buttonPushedCounter--;
			CloseGate();
		}
		else
		{
			_buttonPushedCounter++;
			if (_buttonPushedCounter == _buttons.Length)
			{
				OpenGate();
			}
		}
	}

	private void OpenGate()
	{
		if (_gateState == GateState.Open)
			return;

		MoveGate(true);
	}

	private void CloseGate()
	{
		if (_gateState == GateState.Closed)
			return;

		MoveGate(false);
	}

	private void MoveGate(bool opening)
	{
		if (_gateState != GateState.Moving)
			_gateState = GateState.Moving;

		if (_doorMovingRoutine != null)
			StopCoroutine(_doorMovingRoutine);
		_doorMovingRoutine = StartCoroutine(MoveGateRoutine(opening));
	}

	private IEnumerator MoveGateRoutine(bool opening)
	{
		HashSet<int> done = new HashSet<int>();
		while (done.Count < _doors.Length)
		{
			for (int i = 0; i < _doors.Length; ++i)
			{
				_doors[i].Collider.transform.position = Vector3.MoveTowards(_doors[i].Collider.transform.position, opening ? _doors[i].OpenStatePosition.position : _doors[i].CloseStatePosition, _doorMovingSpeed * Time.deltaTime);
				if (!done.Contains(i) && _doors[i].Collider.transform.position == (opening ? _doors[i].OpenStatePosition.position : _doors[i].CloseStatePosition))
				{
					done.Add(i);
				}
				float distance = Vector3.Distance(_doors[i].Collider.position, _doors[i].OpenStatePosition.position);
				_doors[i].ShaderModifier.UpdateStepValue(distance / _doors[i].MaxDistance);
			}
			yield return null;
		}
		_gateState = opening ? GateState.Open : GateState.Closed;
	}

	[Serializable]
	public struct DoorStruct
	{
		public Transform Collider;
		public Transform OpenStatePosition;
		public DoorShaderModifier ShaderModifier;
		public Vector3 CloseStatePosition { get; set; }
		public float MaxDistance { get; set; }
	}
}

