﻿using System.Collections;
using UnityEngine;

public class EnergyBall : MonoBehaviour 
{

	[SerializeField] private float _speed;
	[SerializeField] private Rigidbody _rigidbody;
	[SerializeField] private float _maxLifetime = 60f;

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.layer == LayerMask.NameToLayer("PortalTrigger") && PortalMaster.Instance.ArePortalsActive)
			return;

		if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
		{
			Player player = collision.gameObject.GetComponent<Player>();
			player.Die();
		}
		else if(collision.gameObject.layer == LayerMask.NameToLayer("EnergyBallReceiver"))
		{
			EnergyBallReceiver generator = collision.gameObject.GetComponent<EnergyBallReceiver>();
			generator.TurnOn();
		}
		Destroy(gameObject);
	}

	public void Launch(Vector3 dir)
	{
		_rigidbody.velocity = dir * _speed;
		gameObject.SetActive(true);
		StartCoroutine(SetDelayedDestroy());
	}

	private IEnumerator SetDelayedDestroy()
	{
		yield return new WaitForSeconds(_maxLifetime);
		Destroy(gameObject);
	}

}

