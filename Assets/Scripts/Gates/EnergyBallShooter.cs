﻿using UnityEngine;

public class EnergyBallShooter : MonoBehaviour 
{

	[SerializeField] private EnergyBall _energyBallPrefab;
	[SerializeField] private Transform _launchPosition;
	[SerializeField] private float _shootingPeriod = 5.0f;

	private void Start()
	{
		InvokeRepeating("CreateEnergyBall", _shootingPeriod, _shootingPeriod);
	}


	private void CreateEnergyBall()
	{
		EnergyBall ball = Instantiate(_energyBallPrefab, transform);
		ball.transform.position = _launchPosition.position;
		ball.Launch(_launchPosition.forward);
	}


}

